import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

var router = new Router({
  routes: [
    {path: '/', name: 'login-register', component: resolve => require(['@/pages/login-register.vue'], resolve), meta: { title: '登陆注册' } },
    {path: '/commodities', name: 'commodities', component: resolve => require(['@/pages/commodities.vue'], resolve), meta: { title: '全部商品' } },
    {path: '/home', name: 'home', component: resolve => require(['@/pages/home.vue'], resolve), meta: { title: '首页' } },
    {path: '/fresh', name: 'fresh', component: resolve => require(['@/pages/fresh.vue'], resolve), meta: { title: '尝鲜装' } },
    {path: '/original', name: 'original', component: resolve => require(['@/pages/original.vue'], resolve), meta: { title: '原箱装' } },
    {path: '/GiftKards', name: 'GiftKards', component: resolve => require(['@/pages/GiftKards.vue'], resolve), meta: { title: '礼品卡 '} },
    {path: '/Epurchases', name: 'Epurchases', component: resolve => require(['@/pages/Epurchases.vue'], resolve), meta: { title: '企业购买' } },
  ],

  linkActiveClass: 'active',
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title + ' - 花果山'
  }
  next()
})

export default router

